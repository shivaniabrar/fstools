#!/usr/bin/env bash
printf "Running inode bitmap tests\n"
inodes_before_tests=($(./inode_walker -f ${1}  | grep -i "Total allocated Inodes:" | awk 'BEGIN{FS=":"} {print $2}'))
printf "Inodes before running tests: %d\n" "${inodes_before_tests}"
printf "Damage Inode Bitmap\n"
./setbitmap -f $1 -i 1 > /dev/null
inodes_after_damage=($(./inode_walker -f ${1}| grep -i "Total allocated Inodes:" | awk 'BEGIN{FS=":"} {print $2}'))
printf "Inodes after damaging inode bitmap: %d\n" "${inodes_after_damage}"
printf "Fixing Inode Bitmap...\n"
./fixbitmap -f $1 -i > /dev/null
inodes_after_fix=($(./inode_walker -f ${1} | grep -i "Total allocated Inodes:" | awk 'BEGIN{FS=":"} {print $2}'))
printf "Inodes after fix inode bitmap: %d\n" "${inodes_after_fix}"
