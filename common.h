#ifndef COMMON_H
#define COMMON_H

typedef enum bool {
    false, true, 
} bool;


typedef enum status_t {
    ok,
    err_null,
} status_t;


//#define DEBUG
#ifdef DEBUG
#define LOG(...) \
do { \
printf(__VA_ARGS__); \
} while(0)
#else
#define LOG(...)
#endif

#endif