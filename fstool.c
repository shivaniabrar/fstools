#include "common.h"

#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <minix/config.h>
#include <minix/const.h>
#include <minix/type.h>
#include "mfs/const.h"
#include "mfs/inode.h"
#include "mfs/type.h"
#include "mfs/mfsdir.h"
#include <minix/fslib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>

#include "mfs_structs.h"
int
main(int argc, char **argv)
{
    int device = 0;
    bitchunk_t *ibitmap, *zbitmap;
    struct super_block superblock;
    char opt = 0;
    bool obtained_filename = false;
    struct direct dp;
    dp.mfs_d_ino = 100;
    strcpy(dp.mfs_d_name, "mydir");

    status_t status = ok;
    if (argc < 2) {
        printf("usage: directory_walker -f filename\n");
        return -1;
    }

    while ((opt = getopt (argc, argv, "f:")) != -1) {
        switch (opt) {
            case 'f':
                obtained_filename = true;
                break;
            default:
                printf("usage: directory_walker -f filename\n");
                return -1;
        }
    }

    if (obtained_filename == false) {
        perror("Error");
        printf("usage: directory_walker -f filename\n");
        fatal("require device path");

    }

    if ((device = open(optarg, O_RDWR)) == -1) {
        perror("Could not open device file");
        exit(-1);
    }

    //dev_imp = alloc_bitmap(imap);
    read_super_block(device, &superblock);
    list_super(&superblock);
    get_bitmaps(&superblock);
    load_bitmap(device, imap, BLK_IMAP, N_IMAP((&superblock)));
    load_bitmap(device, zmap, BLK_ZMAP((&superblock)), N_ZMAP((&superblock)));
    ibitmap = alloc_bitmap(superblock.s_imap_blocks);
    zbitmap = alloc_bitmap(superblock.s_zmap_blocks);

    printf("Expected Inode count: %llu\n", count_bits(set_bits, imap, N_IMAP((&superblock))));
    custom_directory_walker(device, &superblock, ROOT_INODE, "", 0, create_inode_bitmap, ibitmap);
    printf("Actual Inode count: %llu\n", count_bits(set_bits, ibitmap, N_IMAP((&superblock))));
    printf("Check Imap\n");
    checkbitmap(imap, ibitmap, N_IMAP((&superblock)), BLOCKSIZE, superblock.s_ninodes, print_diff_bit, NULL);
    printf("Expected Walk Inode\n");
    inode_walker(device, &superblock, imap, N_IMAP((&superblock)));
    printf("Actual Walk Inode\n");
    inode_walker(device, &superblock, ibitmap, N_IMAP((&superblock)));

    //inode_walker(device, &superblock, imap, N_IMAP((&superblock)));
    // Allocated Meta Data Zones
    //zone_walker(device, &superblock, zmap, N_ZMAP((&superblock)));
    //custom_directory_walker(device, &superblock, ROOT_INODE, "", 0, directory_walker_print, NULL);

    printf("Zone Count from Zone Bit map: %llu\n", count_bits(set_bits, zmap, N_ZMAP((&superblock))));
    custom_inode_walker(device, &superblock, imap, N_IMAP((&superblock)), create_zone_bitmap, zbitmap);
    printf("Zone Count from Inode Walker: %llu\n", count_bits(set_bits, zbitmap, N_ZMAP((&superblock))));
    printf("Check Zmap\n");
    checkbitmap(zmap, zbitmap, N_ZMAP((&superblock)), BLOCKSIZE, superblock.s_nzones, &print_diff_bit, NULL);
    printf("Zone BitMap\n");
    zone_walker(device, &superblock, zmap, N_ZMAP((&superblock)));
    printf("Inode Walker: Zone BitMap\n");
    zone_walker(device, &superblock, zbitmap, N_ZMAP((&superblock)));

    write_directory_entry(device, &superblock, ROOT_INODE, &dp);

    free_bitmap(ibitmap);
    free_bitmap(zbitmap);
    free_bitmap(imap);
    free_bitmap(zmap);
    close(device);
    return 0;

}

