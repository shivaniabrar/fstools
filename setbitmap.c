//
// Created by Abrar Shivani on 4/25/17.
//

#include "common.h"

#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <minix/config.h>
#include <minix/const.h>
#include <minix/type.h>
#include "mfs/const.h"
#include "mfs/inode.h"
#include "mfs/type.h"
#include "mfs/mfsdir.h"
#include <minix/fslib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>

#include "mfs_structs.h"

#define CMD_USAGE \
printf("usage: setbitmap -f filename -i inode number -z zone number\n")



int
main(int argc, char **argv)
{
    int device = 0;
    struct super_block superblock;
    char opt = 0;

    bool obtained_filename = false;
    bool obtained_inode_number = false;
    bool obtained_zone_number = false;
    bool obtained_set_bitmap = false;

    char *device_path = NULL;
    uint64_t inode_number = 0;
    uint64_t zone_number = 0;

    status_t status = ok;
    if (argc < 3) {
        CMD_USAGE;
        return -1;
    }

    while ((opt = getopt (argc, argv, "f:i:z:s")) != -1) {
        switch (opt) {
            case 'f':
                obtained_filename = true;
                device_path = strdup(optarg);
                break;
            case 'i':
                obtained_inode_number = true;
                inode_number = atoi(optarg);
                break;
            case 'z':
                obtained_zone_number = true;
                zone_number = atoi(optarg);
                break;
            case 's':
                obtained_set_bitmap = true;
                break;
            default:
                CMD_USAGE;
                return -1;
        }
    }

    if (obtained_filename == false) {
        perror("Error");
        CMD_USAGE;
        fatal("require device path");
    }

    if (obtained_set_bitmap == false) {
        printf("Info: unset operation\n");
    }

    if (obtained_inode_number == false
        && obtained_zone_number == false) {
        perror("Error");
        CMD_USAGE;
        fatal("require either inode number or device path");
    }

    if ((device = open(device_path, O_RDWR)) == -1) {
        perror("Could not open device file");
        exit(-1);
    }

    read_super_block(device, &superblock);
    list_super(&superblock);
    get_bitmaps(&superblock);

    load_bitmap(device, imap, BLK_IMAP, N_IMAP((&superblock)));
    load_bitmap(device, zmap, BLK_ZMAP((&superblock)), N_ZMAP((&superblock)));



    if (obtained_inode_number == true) {
        if (obtained_set_bitmap) {
            setbitmap(imap, superblock.s_imap_blocks, superblock.s_block_size, inode_number);
        } else {
            unsetbitmap(imap, superblock.s_imap_blocks, superblock.s_block_size, inode_number);
        }
        put_bitmap(device, imap, BLK_IMAP, superblock.s_imap_blocks);
    }

    if (obtained_zone_number == true) {
        if (obtained_set_bitmap) {
            setbitmap(zmap, superblock.s_zmap_blocks, superblock.s_block_size, zone_number);
        } else {
            unsetbitmap(zmap, superblock.s_zmap_blocks, superblock.s_block_size, zone_number);
        }
        put_bitmap(device, zmap, BLK_ZMAP((&superblock)), superblock.s_zmap_blocks);
    }



    free(imap);
    free(zmap);
    close(device);
}

