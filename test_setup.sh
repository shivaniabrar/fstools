vnd=($(vndconfig -l | grep -i "not in use" | awk 'BEGIN{FS=":"} {print $1}'))
tot=($(vndconfig -l | grep -i "not in use" | wc -l))

if [ "$tot" -eq "0" ]; then
    printf "Error: No logical device available\n"
    printf "Please free logical device and run script again\n"
fi

for i in `seq 1 $tot`
do
    printf "Adding $1 as logical device %s\n" "${vnd[$i]}"
    if vndconfig ${vnd[$i]} $1; then
        printf "done\n"
        break
    else
        printf "Failed to add device %s\n" "${vnd[$i]}"
    fi
done



