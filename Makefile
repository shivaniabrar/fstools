FSTOOL=  fstool
PROG=	fstool
SRCS=	mfs_structs.c fstool.c
CPPFLAGS+= -Wno-error -I${NETBSDSRCDIR}/minix/fs -I${FSTOOL}
MAN=

all: inode_walker zone_walker setbitmap fixbitmap dirwalker corrupt_dir fixdir

inode_walker:
	clang -o inode_walker inode_walker.c mfs_structs.c

zone_walker:
	clang -o zone_walker zone_walker.c mfs_structs.c

setbitmap:
	clang -o setbitmap setbitmap.c mfs_structs.c

fixbitmap:
	clang -o fixbitmap fixbitmap.c mfs_structs.c

dirwalker:
	clang -o dir_walker dir_walker.c mfs_structs.c

corrupt_dir:
	clang -o corrupt_dir corrupt_dir.c mfs_structs.c

fixdir:
	clang -o fixdir fixdir.c mfs_structs.c

clean:
	rm -rf mfs_structs.o
	rm -rf inode_walker inode_walker.o
	rm -rf zone_walker zone_walker.o
	rm -rf setbitmap setbitmap.o
	rm -rf fixbitmap fixbitmap.o
	rm -rf fixdir fixdir.o
	rm -rf corrupt_dir corrupt_dir.o

.include <bsd.prog.mk>