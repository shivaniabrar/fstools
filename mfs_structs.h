//
// Created by Abrar Shivani on 4/22/17.
//

#ifndef FSTOOLS_MFS_STRUCTS_H
#define FSTOOLS_MFS_STRUCTS_H


#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <minix/config.h>
#include <minix/const.h>
#include <minix/type.h>
#include "mfs/const.h"
#include "mfs/inode.h"
#include "mfs/type.h"
#include "mfs/mfsdir.h"
#include <minix/fslib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include "common.h"
#define INODE_SIZE ((int) V2_INODE_SIZE)


#define ZONE_TO_BLOCK(z, superblock)	((block_nr) (z) << (superblock->s_log_zone_size))
#define ZONE_SIZE(superblock)	((int) ZONE_TO_BLOCK(BLOCKSIZE))
#define SET_BLOCK_SIZE(BLK_SIZE) (block_size = BLK_SIZE)
#define BLOCKSIZE (block_size)
#define WORDS_PER_BLOCK (block_size / (int) sizeof(bitchunk_t))
#define BLOCK_ADDRESS(BLKNO) ((u64_t)(BLKNO) * block_size)
#define OFFSET_SUPER_BLOCK	SUPER_BLOCK_BYTES
#define block_nr block_t

#define N_IMAP(superblock) (superblock->s_imap_blocks)
#define N_ZMAP(superblock) (superblock->s_zmap_blocks)

/* Block address of each type */
#define BLK_IMAP	2
#define BLK_ZMAP(superblock)	(BLK_IMAP  + N_IMAP(superblock))
#define BLK_ILIST(superblock)	(BLK_ZMAP(superblock)  + N_ZMAP(superblock))

#define MAX(a,b)\
(((a)>(b))?(a):(b))

#define INODE_BLOCKS(inode) \
  ( div2(inode->d2_size, BLOCKSIZE) )


#define LSEEK(DEVICE, OFFSET, SEEK_OFFSET) \
if(lseek(DEVICE, OFFSET, SEEK_OFFSET) < 0) { \
    perror("lseek"); \
    fatal("couldn't seek to block.");\
}

#define READ(DEVICE, BUFFER, SIZE)\
if(read(DEVICE, BUFFER, SIZE) != SIZE) { \
    fatal("couldn't read block."); \
}

#define WRITE(DEVICE, BUFFER, SIZE)\
if(write(DEVICE, BUFFER, SIZE) != SIZE) { \
    fatal("couldn't write block."); \
}

#define PRINT_NEW_LINE (printf("\n"))

#define IS_DIR_INODE(inode) \
((inode->d2_mode  & S_IFMT) == S_IFDIR)

typedef struct {
    int device;
    struct super_block *super_block;
    uint64_t depth;
    uint64_t inode_number;
    char *filename;
    char *dirname;
    bool is_dir;
    uint64_t block_number;
} dir_info;


typedef struct {
    int device;
    struct super_block *superblock;
} dir_root;

typedef struct {
    char *path;
    uint64_t inode_number;
} path_inode_t;

extern unsigned int fs_version;
extern unsigned int block_size;
extern bitchunk_t *imap;
extern bitchunk_t *zmap;

void fatal(char *s);
unsigned div2(unsigned num1, unsigned num2);

void read_super_block(int device, struct super_block *superblock);
void write_super_block(int device, struct super_block *superblock);
void validate_super_block(int device, struct super_block *superblock);
void list_super(struct super_block *superblock);

char *alloc(unsigned nelem, unsigned elsize);
bitchunk_t *alloc_bitmap(int nblk);
void free_bitmap(bitchunk_t *bitmap);
void load_bitmap(int device, bitchunk_t *bitmap, block_nr bno, int nblk);
void get_bitmaps(struct super_block *superblock);
void put_bitmaps();
void read_block(int device, int bno, char *buf, int block_size);
void write_block(int device, int bno, char *buf, int block_size);

uint64_t count_bits(int (*bits_type)(int bit), bitchunk_t *bitmap, int nblk);
int count_bits_in_word(int (*bits_type)(int bit), bitchunk_t *bitmap);
int total_bits(int bit);
int set_bits(int bit);

int inode_walker(int device, struct super_block *super_block, bitchunk_t *bitmap, int nblk);
void read_block_from_offset(int device, int bno, int offset, char *buf, int block_size, int size);
void get_inode(int device, struct super_block *super_block, int inode_number, d2_inode *inode);
int get_inode_block(struct super_block *superblock, int inode_number);
int get_inode_offset(int inode_number);
int print_inode(int inode_number, d2_inode *inode);
int zone_walker(int device, struct super_block *super_block, bitchunk_t *bitmap, int nblk);
int directory_walker (int device, struct super_block *superblock);
bool check_root_inode_is_directory(int device, struct super_block *superblock);
bool check_inode_represents_directory(int device, struct super_block *superblock, int inode_number);

int convert_logical_to_physical_block (int device,
                                   struct super_block *superblock,
                                   d2_inode *d2inode,
                                   unsigned log_block,
                                   unsigned *ph_block);
int
_directory_walker (int device, struct super_block *superblock, uint64_t inode_number, char *dirname, int depth);

typedef int (*do_fn)(dir_info *, void *);

int
custom_directory_walker (int device,
                         struct super_block *superblock,
                         uint64_t inode_number,
                         char *dirname,
                         int depth,
                         do_fn fn,
                         void *do_arg);

int
custom_inode_walker (int device,
                     struct super_block *superblock,
                     bitchunk_t *bitmap,
                     int nblk,
                     int (*fn) (int, struct super_block *, uint64_t , d2_inode *, void *),
                     void *arg);

int directory_walker_print(dir_info *info, void *arg);
void setbitmap(bitchunk_t *bitmap, int nblocks, int blocksize, bit_t bitnr);
int create_inode_bitmap(dir_info *info, void *arg);
int create_zone_bitmap(int device, struct super_block *superblock, uint64_t inode_number, d2_inode *d2inode, void *arg);
int create_zone_bitmap_helper(struct super_block *superblock, zone_t *zones, bitchunk_t *bitmap, uint64_t nzones);
int checkbitmap(bitchunk_t *source,
            bitchunk_t *dest,
            int nblocks,
            int blocksize,
            int maxsize,
            int (*fn)(bit_t, void *),
            void *arg);
int print_diff_bit(bit_t number, void *arg);
int put_bitmap(int device, bitchunk_t *bitmap, block_nr bno, int nblk);
void unsetbitmap(bitchunk_t *bitmap, int nblocks, int blocksize, bit_t bitnr);
void write_directory_entry(int device, struct super_block *superblock, uint32_t inode_number,  struct direct *dp);
void rand_str(char *dest, size_t length);
int create_dir_ent(bit_t inode_number, struct direct *dp);
int find_inode_for_path(dir_info *info, void *arg);
int corrupt_directory(int device, struct super_block *superblock, char *dirname);
void write_inode(int device, struct super_block *superblock, int inode_number, d2_inode *inode);
int add_to_entry_to_root_dir(bit_t inode_number, void *arg);
int get_inode_index(int inode_number);
#endif //FSTOOLS_MFS_STRUCTS_H
