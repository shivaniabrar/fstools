#!/usr/bin/env bash
printf "Running zone bitmap tests\n"
zones_before_tests=($(./zone_walker -f ${1}  | grep -i "Total allocated Zones:" | awk 'BEGIN{FS=":"} {print $2}'))
printf "Zones before running tests: %d\n" "${zones_before_tests}"
printf "Damage Zone Bitmap\n"
./setbitmap -f $1 -z 1 > /dev/null
zones_after_damage=($(./zone_walker -f ${1}| grep -i "Total allocated Zones:" | awk 'BEGIN{FS=":"} {print $2}'))
printf "Zones after damaging zone bitmap: %d\n" "${zones_after_damage}"
printf "Fixing Zone Bitmap...\n"
./fixbitmap -f $1 -z > /dev/null
zones_after_fix=($(./zone_walker -f ${1} | grep -i "Total allocated Zones:" | awk 'BEGIN{FS=":"} {print $2}'))
printf "Zones after fix zone bitmap: %d\n" "${zones_after_fix}"
