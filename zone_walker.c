#include "common.h"

#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <minix/config.h>
#include <minix/const.h>
#include <minix/type.h>
#include "mfs/const.h"
#include "mfs/inode.h"
#include "mfs/type.h"
#include "mfs/mfsdir.h"
#include <minix/fslib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>

#include "mfs_structs.h"
int
main(int argc, char **argv)
{
    int device = 0;
    struct super_block superblock;
    char opt = 0;
    bool obtained_filename = false;
    //bitchuck_t *dev_imap;
    status_t status = ok;
    if (argc < 2) {
        printf("usage: directory_walker -f filename\n");
        return -1;
    }

    while ((opt = getopt (argc, argv, "f:")) != -1) {
        switch (opt) {
            case 'f':
                obtained_filename = true;
                break;
            default:
                printf("usage: zone_walker -f filename\n");
                return -1;
        }
    }

    if (obtained_filename == false) {
        printf("usage: zone_walker -f filename\n");
        fatal("require device path");

    }

    if ((device = open(optarg, O_RDWR)) == -1) {
        perror("Could not open device file");
        exit(-1);
    }

    read_super_block(device, &superblock);
    list_super(&superblock);
    get_bitmaps(&superblock);
    load_bitmap(device, zmap, BLK_ZMAP((&superblock)), N_ZMAP((&superblock)));
    printf("\nTotal allocated zones: %llu\n", count_bits(set_bits, zmap, N_ZMAP((&superblock))) - 1);
    printf("\nTotal zones allocated for metadata: %d\n", superblock.s_firstdatazone_old);
    zone_walker(device, &superblock, zmap, N_ZMAP((&superblock)));
    free_bitmap(imap);
    free_bitmap(zmap);
    close(device);
    return 0;

}

