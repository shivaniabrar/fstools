//
// Created by Abrar Shivani on 4/25/17.
//

#include "common.h"

#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <minix/config.h>
#include <minix/const.h>
#include <minix/type.h>
#include "mfs/const.h"
#include "mfs/inode.h"
#include "mfs/type.h"
#include "mfs/mfsdir.h"
#include <minix/fslib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>

#include "mfs_structs.h"

#define CMD_USAGE \
printf("usage: fixbitmap -f filename -i -z\n")



int
main(int argc, char **argv)
{
    int device = 0;
    struct super_block superblock;
    bitchunk_t *ibitmap, *zbitmap;
    char opt = 0;

    bool obtained_filename = false;
    bool obtained_inode_arg = false;
    bool obtained_zone_arg = false;

    char *device_path = NULL;
    uint64_t inode_number = 0;
    uint64_t zone_number = 0;

    status_t status = ok;
    if (argc < 2) {
        CMD_USAGE;
        return -1;
    }

    while ((opt = getopt (argc, argv, "f:iz")) != -1) {
        switch (opt) {
            case 'f':
                obtained_filename = true;
                device_path = strdup(optarg);
                break;
            case 'i':
                obtained_inode_arg = true;
                break;
            case 'z':
                obtained_zone_arg = true;
                break;
            default:
                CMD_USAGE;
                return -1;
        }
    }

    if (!obtained_filename) {
        CMD_USAGE;
        fatal("require device path");
    }

    if ((device = open(device_path, O_RDWR)) == -1) {
        perror("Could not open device file");
        exit(-1);
    }

    read_super_block(device, &superblock);
    list_super(&superblock);
    get_bitmaps(&superblock);

    load_bitmap(device, imap, BLK_IMAP, N_IMAP((&superblock)));
    load_bitmap(device, zmap, BLK_ZMAP((&superblock)), N_ZMAP((&superblock)));

    ibitmap = alloc_bitmap(superblock.s_imap_blocks);
    zbitmap = alloc_bitmap(superblock.s_zmap_blocks);


    if (obtained_inode_arg) {
        custom_directory_walker(device, &superblock, ROOT_INODE, "", 0, create_inode_bitmap, ibitmap);
        checkbitmap(imap, ibitmap, N_IMAP((&superblock)), BLOCKSIZE, superblock.s_ninodes, &print_diff_bit, NULL);
        put_bitmap(device, ibitmap, BLK_IMAP, superblock.s_imap_blocks);
    }

    if (obtained_zone_arg) {
        custom_directory_walker(device, &superblock, ROOT_INODE, "", 0, create_inode_bitmap, ibitmap);
        custom_inode_walker(device, &superblock, ibitmap, N_IMAP((&superblock)), create_zone_bitmap, zbitmap);
        checkbitmap(zmap, zbitmap, N_ZMAP((&superblock)), BLOCKSIZE, superblock.s_zones, &print_diff_bit, NULL);
        put_bitmap(device, zbitmap, BLK_ZMAP((&superblock)), superblock.s_zmap_blocks);
    }


    free(imap);
    free(zmap);
    free(ibitmap);
    free(zbitmap);
    close(device);
}

