//
// Created by Abrar Shivani on 4/25/17.
//

#include "mfs_structs.h"
#include "common.h"
#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <minix/config.h>
#include <minix/const.h>
#include <minix/type.h>
#include "mfs/const.h"
#include "mfs/inode.h"
#include "mfs/type.h"
#include "mfs/mfsdir.h"
#include <minix/fslib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include <assert.h>


#define CMD_USAGE \
printf("usage: fixdir -f device\n")

int
main(int argc, char **argv) {
    int device = 0;
    struct super_block superblock;
    char opt = 0;
    dir_root dirinfo;

    bool obtained_filename = false;
    bool obtained_dir = false;
    bitchunk_t *ibitmap;

    char *device_path = NULL;
    char *dirpath = NULL;

    status_t status = ok;
    if (argc < 2) {
        CMD_USAGE;
        return -1;
    }

    while ((opt = getopt(argc, argv, "f:")) != -1) {
        switch (opt) {
            case 'f':
                obtained_filename = true;
                device_path = strdup(optarg);
                break;
            default:
                CMD_USAGE;
                return -1;
        }
    }

    if (obtained_filename == false) {
        CMD_USAGE;
        fatal("require device path");
    }

    if ((device = open(device_path, O_RDWR)) == -1) {
        perror("Could not open device file");
        exit(-1);
    }

    read_super_block(device, &superblock);
    get_bitmaps(&superblock);
    load_bitmap(device, imap, BLK_IMAP, N_IMAP((&superblock)));
    dirinfo.device = device;
    dirinfo.superblock = &superblock;
    ibitmap = alloc_bitmap(superblock.s_imap_blocks);
    custom_directory_walker(device, &superblock, ROOT_INODE, "", 0, create_inode_bitmap, ibitmap);
    printf("Done\n");
    checkbitmap(imap, ibitmap, N_IMAP((&superblock)), BLOCKSIZE, superblock.s_ninodes, &add_to_entry_to_root_dir, &dirinfo);
    free(imap);
    free(zmap);
    free(ibitmap);
    close(device);
}



