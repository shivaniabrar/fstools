//
// Created by Abrar Shivani on 4/22/17.
//

#include "mfs_structs.h"
#include "common.h"
#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <minix/config.h>
#include <minix/const.h>
#include <minix/type.h>
#include "mfs/const.h"
#include "mfs/inode.h"
#include "mfs/type.h"
#include "mfs/mfsdir.h"
#include <minix/fslib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include <assert.h>

unsigned int fs_version = 2;
unsigned int block_size = 0;
bitchunk_t *imap;
bitchunk_t *zmap;

void
read_super_block(int device, struct super_block *superblock)
{
    LSEEK(device, OFFSET_SUPER_BLOCK, SEEK_SET);
    READ(device, superblock, SUPER_SIZE);
    validate_super_block(device, superblock);
    return;
}

void
write_super_block(int device, struct super_block *superblock)
{
    LSEEK(device, OFFSET_SUPER_BLOCK, SEEK_SET);
    WRITE(device, superblock, SUPER_SIZE);
    return;
}

void
validate_super_block(int device, struct super_block *superblock)
{
    if (superblock->s_magic == SUPER_MAGIC) {
        fatal("Cannot handle V1 file systems");
    }
    if (superblock->s_magic == SUPER_V2) {
        fs_version = 2;
        SET_BLOCK_SIZE(8192); /* STATIC_BLOCK_SIZE */
    } else if(superblock->s_magic == SUPER_V3) {
        fs_version = 3;
        SET_BLOCK_SIZE(superblock->s_block_size);
    } else {
        fatal("bad magic number in super block");
    }

    if (superblock->s_ninodes <= 0) {
        fatal("no inodes");
    }
    if (superblock->s_zones <= 0) {
        fatal("no zones");
    }
    if (superblock->s_imap_blocks <= 0) {
        fatal("no imap");
    }
    if (superblock->s_zmap_blocks <= 0) {
        fatal("no zmap");
    }
    if (superblock->s_firstdatazone != 0 && superblock->s_firstdatazone <= 4) {
        fatal("first data zone too small");
    }
    if (superblock->s_log_zone_size < 0) {
        fatal("zone size < block size");
    }
    if (superblock->s_max_size <= 0) {
        printf("warning: invalid max file size %d\n", superblock->s_max_size);
        superblock->s_max_size = LONG_MAX;
    }
}

void list_super(struct super_block *superblock)
{
    do {
        /* Most of the following atol's enrage lint, for good reason. */
        printf("ninodes       = %u", superblock->s_ninodes);
        PRINT_NEW_LINE;
        printf("nzones        = %d", superblock->s_zones);
        PRINT_NEW_LINE;
        printf("imap_blocks   = %u", superblock->s_imap_blocks);
        PRINT_NEW_LINE;
        printf("zmap_blocks   = %u", superblock->s_zmap_blocks);
        PRINT_NEW_LINE;
        printf("firstdatazone = %u", superblock->s_firstdatazone_old);
        PRINT_NEW_LINE;
        printf("log_zone_size = %u", superblock->s_log_zone_size);
        PRINT_NEW_LINE;
        printf("maxsize       = %d", superblock->s_max_size);
        PRINT_NEW_LINE;
        printf("block size    = %d", superblock->s_block_size);
        PRINT_NEW_LINE;
        printf("flags         = ");
        if(superblock->s_flags & MFSFLAG_CLEAN) printf("CLEAN "); else printf("DIRTY ");
        PRINT_NEW_LINE;
    } while(0);
}

char *
alloc(unsigned nelem, unsigned elsize)
{
    char *p;

    if ((p = (char *)malloc((size_t)nelem * elsize)) == 0) {
        LOG("Tried to allocate %dkB\n", nelem*elsize/1024);
        fatal("out of memory");
    }
    memset((void *) p, 0, (size_t)nelem * elsize);
    return(p);
}

bitchunk_t *
alloc_bitmap(int nblk) {
    bitchunk_t *bitmap;
    bitmap = (bitchunk_t *) alloc((unsigned) nblk, block_size);
    *bitmap |= 1;
    return (bitmap);
}

void
load_bitmap(int device, bitchunk_t *bitmap, block_nr bno, int nblk) {
    int i;
    bitchunk_t *p;

    p = bitmap;
    for (i = 0; i < nblk; i++, bno++, p += WORDS_PER_BLOCK)
        read_block(device, bno, (char *) p, BLOCKSIZE);
    *bitmap |= 1;
}

void
free_bitmap(bitchunk_t *bitmap) {
    free(bitmap);
}

void
get_bitmaps(struct super_block *superblock)
{
    imap = alloc_bitmap(N_IMAP(superblock));
    zmap = alloc_bitmap(N_ZMAP(superblock));
}

void
read_block(int device, int bno, char *buf, int block_size)
{
    memset(buf, 0, block_size);
    LSEEK(device, BLOCK_ADDRESS(bno), SEEK_SET);
    READ(device, buf, block_size);
}

void
write_block(int device, int bno, char *buf, int block_size)
{
    LSEEK(device, BLOCK_ADDRESS(bno), SEEK_SET);
    WRITE(device, buf, block_size);
}

uint64_t
count_bits(int (*bits_type)(int bit), bitchunk_t *bitmap, int nblk)
{
    uint64_t total_bitmaps_in_all_blocks = 0;
    uint64_t total_bits = 0;
    total_bitmaps_in_all_blocks = (WORDS_PER_BLOCK) * nblk;
    while(total_bitmaps_in_all_blocks != 0) {
        total_bits += count_bits_in_word(bits_type, bitmap);
        bitmap += 1;
        --total_bitmaps_in_all_blocks;
    }
    return total_bits;
}

int
count_bits_in_word(int (*bits_type)(int bit), bitchunk_t *bitmap)
{
    uint32_t total_bits = 0;
    uint32_t bits_to_read = sizeof(bitchunk_t) * 8;
    bitchunk_t tmp_bitmap = *bitmap;
    while(bits_to_read)
    {
        total_bits += bits_type((tmp_bitmap) & 1);
        tmp_bitmap = (tmp_bitmap) >> 1;
        --bits_to_read;
    }
    return total_bits;
}

int
total_bits(int bit)
{
    return 1;
}

int
set_bits(int bit)
{
    return bit;
}

/* Print the string `s' and exit. */
void fatal(char *s)
{
    printf("\nfatal: %s\n", s);
    exit(-1);
}

int
inode_walker(int device, struct super_block *superblock, bitchunk_t *bitmap, int nblk)
{
    uint64_t total_bitmaps_in_all_blocks = (WORDS_PER_BLOCK) * nblk;
    uint32_t bits_to_read = superblock->s_ninodes;
    uint32_t max_bits_in_chunk = sizeof(bitchunk_t) * 8;
    bit_t bit_nr = 0;
    bitchunk_t tmp_bitmap = *bitmap;
    d2_inode *buf = malloc(INODE_SIZE);
    while(bits_to_read) {
        if ((!(bit_nr % max_bits_in_chunk)) && (bits_to_read !=  superblock->s_ninodes)){
            bitmap += 1;
            tmp_bitmap = *bitmap;
        }
        if (((tmp_bitmap) & 1) == 1) {
            get_inode(device, superblock,  bit_nr, buf);
            print_inode(bit_nr, buf);
        }
        bit_nr += 1;
        tmp_bitmap = tmp_bitmap >> 1;
        --bits_to_read;
    }
    free(buf);
    return 0;
}

int
custom_inode_walker (int device,
                     struct super_block *superblock,
                     bitchunk_t *bitmap,
                     int nblk,
                     int (*fn) (int, struct super_block *, uint64_t , d2_inode *, void *),
                     void *arg)
{
    uint64_t total_bitmaps_in_all_blocks = (WORDS_PER_BLOCK) * nblk;
    uint32_t bits_to_read = superblock->s_ninodes;
    uint32_t max_bits_in_chunk = sizeof(bitchunk_t) * 8;
    bit_t bit_nr = 0;
    bitchunk_t tmp_bitmap = *bitmap;
    d2_inode *buf = malloc(INODE_SIZE);
    while(bits_to_read) {
        if ((!(bit_nr % max_bits_in_chunk)) && (bits_to_read != superblock->s_ninodes)){
            bitmap += 1;
            tmp_bitmap = *bitmap;
        }
        if (((tmp_bitmap) & 1) == 1) {
            get_inode(device, superblock,  bit_nr, buf);
            fn(device, superblock, bit_nr, buf, arg);
        }
        bit_nr += 1;
        tmp_bitmap = tmp_bitmap >> 1;
        --bits_to_read;
    }
    free(buf);
    return 0;
}

int
create_zone_bitmap_helper(struct super_block *superblock, zone_t *zones, bitchunk_t *bitmap, uint64_t nzones)
{
    for (int zone = 0; zone < nzones; zone++) {
        if (zones[zone] == 0) {
            return 0;
        }
        LOG("Zones: %d\n", zones[zone]);
        setbitmap(bitmap, superblock->s_zmap_blocks, superblock->s_block_size, zones[zone] - (superblock->s_firstdatazone_old - 1));
    }
    return 1;
}


int
create_zone_bitmap(int device,
                   struct super_block *superblock,
                   uint64_t inode_number,
                   d2_inode *d2inode,
                   void *arg)
{

    zone_t *dblock_array, *tblock_array, *fblock_array;
    uint64_t blocks = 0;
    dblock_array = malloc(BLOCKSIZE);
    tblock_array = malloc(BLOCKSIZE);
    fblock_array = malloc(BLOCKSIZE);

    create_zone_bitmap_helper(superblock, d2inode->d2_zone, (bitchunk_t *)arg, V2_NR_TZONES);
    if (d2inode->d2_zone[V2_NR_DZONES] == 0) {
        free(dblock_array);
        free(tblock_array);
        free(fblock_array);
        return 0;
    }

    do { // Indirect
        read_block(device, d2inode->d2_zone[V2_NR_DZONES], (char *)dblock_array, BLOCKSIZE);
        create_zone_bitmap_helper(superblock, dblock_array, (bitchunk_t *) arg, (V2_INDIRECTS(superblock->s_block_size)));
    } while(0);

    if (d2inode->d2_zone[V2_NR_DZONES + 1] == 0) {
        free(dblock_array);
        free(tblock_array);
        free(fblock_array);
        return 0;
    }

    do { // Double Indirect
        read_block(device, d2inode->d2_zone[V2_NR_DZONES + 1], (char *)dblock_array, BLOCKSIZE);
        create_zone_bitmap_helper(superblock, dblock_array, (bitchunk_t *) arg, (V2_INDIRECTS(superblock->s_block_size)));
        for (blocks = 0; blocks < (V2_INDIRECTS(superblock->s_block_size)); ++blocks) {
            if (dblock_array[blocks] == 0) {
                free(dblock_array);
                free(tblock_array);
                free(fblock_array);
                return 0;
            }
            read_block(device, dblock_array[blocks], (char *)tblock_array, BLOCKSIZE);
            create_zone_bitmap_helper(superblock, tblock_array, (bitchunk_t *) arg, (V2_INDIRECTS(superblock->s_block_size)));
        }
    } while(0);

    if (d2inode->d2_zone[V2_NR_DZONES + 2] == 0) {
        free(dblock_array);
        free(tblock_array);
        free(fblock_array);
        return 0;
    }

    do { // Triple Indirect
        read_block(device, d2inode->d2_zone[V2_NR_DZONES + 2], (char *)dblock_array, BLOCKSIZE);
        create_zone_bitmap_helper(superblock, dblock_array, (bitchunk_t *) arg, (V2_INDIRECTS(superblock->s_block_size)));
        for (blocks = 0; blocks < (V2_INDIRECTS(superblock->s_block_size)); ++blocks) {
            read_block(device, dblock_array[blocks], (char *)tblock_array, BLOCKSIZE);
            create_zone_bitmap_helper(superblock, tblock_array, (bitchunk_t *) arg, (V2_INDIRECTS(superblock->s_block_size)));
            for (uint64_t tblocks = 0; tblocks < (V2_INDIRECTS(superblock->s_block_size)); ++tblocks) {
                if (tblock_array[tblocks] == 0) {
                    free(dblock_array);
                    free(tblock_array);
                    free(fblock_array);
                    return 0;
                }
                read_block(device, tblock_array[tblocks], (char *)fblock_array, BLOCKSIZE);
                create_zone_bitmap_helper(superblock, fblock_array, (bitchunk_t *) arg, (V2_INDIRECTS(superblock->s_block_size)));
            }
        }

    } while(0);

    free(dblock_array);
    free(tblock_array);
    free(fblock_array);
    return 0;
}


int
checkbitmap(bitchunk_t *source,
            bitchunk_t *dest,
            int nblocks,
            int blocksize,
            int maxsize,
            int (*fn)(bit_t, void *),
            void *arg)
{
    uint64_t maxbits = nblocks * blocksize * 8;
    bitchunk_t sword = *source, dword = *dest;
    uint64_t sbit, dbit;
    int i = 0;
    if (maxsize > maxbits) {
        printf("Max Reached");
        return -1;
    }

    for (uint64_t i = 0; i < maxsize; i++) {
        sbit = sword & 1;
        dbit = dword & 1;
        if ( sbit != dbit ) {
            printf("source:%u dest:%u\n",sword & 1, dword & 1);
            if (fn(i, arg) == -1) {
                return -1;
            }
        }
        sword = sword >> 1;
        dword = dword >> 1;
        if ((i != maxsize && i != 0) &&
                ((i % (sizeof(bitchunk_t) * 8)) == 0)) {
            ++source;
            ++dest;
            sword = *source;
            dword = *dest;
        }
    }
    return 0;
}



int
add_to_entry_to_root_dir(bit_t inode_number, void *arg)
{
    struct direct dir;
    dir_root *dirinfo = (dir_root *) arg;
    LOG("I%d\n", inode_number);
    if (inode_number == 0) {
        return 0;
    }
    create_dir_ent(inode_number, &dir);
    write_directory_entry(dirinfo->device, dirinfo->superblock, ROOT_INODE, &dir);
    return 0;
}

int
create_dir_ent(bit_t inode_number, struct direct *dp)
{
    dp->mfs_d_ino = inode_number;
    rand_str(dp->mfs_d_name, 60);
    return 0;
}

void rand_str(char *dest, size_t length) {
    char charset[] = "0123456789"
            "abcdefghijklmnopqrstuvwxyz"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    while (length-- > 0) {
        size_t index = (double) rand() / RAND_MAX * (sizeof charset - 1);
        *dest++ = charset[index];
    }
    *dest = '\0';
}

int
print_diff_bit(bit_t number, void *arg)
{
    printf("Diff Bit: %d\n", number);
    return 0;
}

int
put_bitmap(int device, bitchunk_t *bitmap, block_nr bno, int nblk)
{
    int i;
    bitchunk_t *p = bitmap;

    for (i = 0; i < nblk; i++, bno++, p += WORDS_PER_BLOCK) {
        write_block(device, bno, (char *) p, block_size);
    }
    return 0;
}



int
directory_walker (int device, struct super_block *superblock)
{
    char *buf = malloc(BLOCKSIZE);
    struct direct *dp;
    struct direct *entries;
    unsigned ph_block;
    d2_inode root_inode;
    get_inode(device, superblock, ROOT_INODE, &root_inode);
    if (IS_DIR_INODE((&root_inode)) == false) {
        fatal("root inode should be directory");
    }
    read_block(device, root_inode.d2_zone[0], buf, superblock->s_block_size) ;
    dp = (struct direct *)buf;
    printf("%d: %s", dp->mfs_d_ino, dp->mfs_d_name);
    convert_logical_to_physical_block(device, superblock, &root_inode, 0, &ph_block);
    read_block(device, ph_block, buf, superblock->s_block_size);
    entries = (struct direct *)buf;
    for (int dentries = 0; dentries < NR_DIR_ENTRIES(BLOCKSIZE); ++dentries) {
        if((entries[dentries]).mfs_d_ino == 0) {
            break;
        }
        printf("%d: %s\n", (entries[dentries]).mfs_d_ino, (entries[dentries]).mfs_d_name);
    }
    free(buf);
    return 0;
}

int
_directory_walker (int device, struct super_block *superblock, uint64_t inode_number, char *dirname, int depth)
{
    uint64_t logical_blocks = 0;
    char *buf = malloc(superblock->s_block_size);
    struct direct *entries;
    unsigned ph_block;
    d2_inode root_inode, dir_inode;
    char path[1024];
    int len = 0;
    get_inode(device, superblock, inode_number, &root_inode);
    if (IS_DIR_INODE((&root_inode)) == false) {
        printf("%*s- %s (inode number: %llu)\n", depth * 2, "", dirname, inode_number);
        free(buf);
        return -1;
    }
    LOG("Dirname: %s - depth = %d\n", dirname, depth);
    for (logical_blocks = 0; logical_blocks < INODE_BLOCKS((&root_inode)); ++logical_blocks)
    {
        convert_logical_to_physical_block(device, superblock, &root_inode, logical_blocks, &ph_block);
        read_block(device, ph_block, buf, superblock->s_block_size);
        entries = (struct direct *)buf;
        for (int dentries = 0; dentries < NR_DIR_ENTRIES(BLOCKSIZE); ++dentries) {
            if((entries[dentries]).mfs_d_ino == 0) {
                LOG("%d: break\n", NR_DIR_ENTRIES(BLOCKSIZE));
                continue;
            }
            LOG("Dirname: %s - depth = %d\n", entries[dentries].mfs_d_name, depth);
            if ( strcmp((entries[dentries]).mfs_d_name, ".") != 0 && strcmp((entries[dentries]).mfs_d_name, "..") != 0) {
                get_inode(device, superblock, (entries[dentries]).mfs_d_ino, &dir_inode);
                if (IS_DIR_INODE((&dir_inode)) == false) {
                    printf("%*s- %s (inode number: %u)\n", depth * 2, "", (entries[dentries]).mfs_d_name, (entries[dentries]).mfs_d_ino);
                } else {
                    len = snprintf(path, sizeof(path)-1, "%s/%s", dirname, (entries[dentries]).mfs_d_name);
                    path[len] = 0;
                    printf("%*s[%s] (inode number: %u) \n", depth*2, "", (entries[dentries]).mfs_d_name, (entries[dentries]).mfs_d_ino);
                    _directory_walker(device, superblock, (entries[dentries]).mfs_d_ino, path, depth + 1);
                }
            }
        }
    }


    free(buf);
    return 0;
}

int
custom_directory_walker (int device,
                   struct super_block *superblock,
                   uint64_t inode_number,
                   char *dirname,
                   int depth,
                   do_fn fn,
                   void *do_arg)
{
    uint64_t logical_blocks = 0;
    char *buf = malloc(superblock->s_block_size);
    struct direct *entries;
    unsigned ph_block;
    d2_inode root_inode, dir_inode;
    char path[1024];
    int len = 0;
    dir_info info;
    info.device = device;
    info.super_block = superblock;

    get_inode(device, superblock, inode_number, &root_inode);
    if (IS_DIR_INODE((&root_inode)) == true) {
        info.depth = depth;
        info.filename = dirname;
        info.inode_number = inode_number;
        info.dirname = path;
        info.is_dir = true;
        fn(&info, do_arg);
    }
    LOG("Dirname: %s - depth = %d\n", dirname, depth);
    for (logical_blocks = 0; logical_blocks < INODE_BLOCKS((&root_inode)); ++logical_blocks)
    {
        convert_logical_to_physical_block(device, superblock, &root_inode, logical_blocks, &ph_block);
        read_block(device, ph_block, buf, superblock->s_block_size);
        entries = (struct direct *)buf;
        for (int dentries = 0; dentries < NR_DIR_ENTRIES(BLOCKSIZE); ++dentries) {
            if((entries[dentries]).mfs_d_ino == 0) {
                LOG("%d: break\n", NR_DIR_ENTRIES(BLOCKSIZE));
                continue;
            }
            LOG("Dirname: %s - depth = %d\n", entries[dentries].mfs_d_name, depth);
            if ( strcmp((entries[dentries]).mfs_d_name, ".") != 0 && strcmp((entries[dentries]).mfs_d_name, "..") != 0) {
                get_inode(device, superblock, (entries[dentries]).mfs_d_ino, &dir_inode);
                LOG("Dirwalker - Inode %d: size: %d, zone0: %d", (entries[dentries]).mfs_d_ino, dir_inode.d2_size, dir_inode.d2_zone[0]);
                info.depth = depth;
                info.filename = (entries[dentries]).mfs_d_name;
                info.inode_number = (entries[dentries]).mfs_d_ino;
                info.block_number = ph_block;
                info.dirname = path;
                if ((IS_DIR_INODE((&dir_inode)) == false)) {
                    info.is_dir = false;
                    fn(&info, do_arg);
                } else {
                    len = snprintf(path, sizeof(path)-1, "%s/%s", dirname, (entries[dentries]).mfs_d_name);
                    path[len] = 0;
                    custom_directory_walker(device, superblock, (entries[dentries]).mfs_d_ino, path, depth + 1, fn, do_arg);
                }
            }
        }
    }
    free(buf);
    return 0;
}

int
directory_walker_print(dir_info *info, void *arg)
{
    if (info->is_dir == true) {
        printf("%*s- %s (inode number: %llu)\n", (MAX(((int)info->depth - 1), 0)) * 2, "", info->filename, info->inode_number);
    } else {
        printf("%*s[%s] (inode number: %llu) \n", (int)info->depth * 2, "", info->filename, info->inode_number);
    }
    return 0;
}

int
find_inode_for_path(dir_info *info, void *arg)
{
    path_inode_t *path_inode = arg;
    if (info->is_dir == true) {
        LOG("Path: walker:%s <-> dir:%s\n", info->filename, path_inode->path);
        if (strcmp(info->filename, path_inode->path) == 0) {
            path_inode->inode_number = info->inode_number;
        }
    }
    return 0;
}

int create_inode_bitmap(dir_info *info, void *arg)
{
   setbitmap((bitchunk_t *) arg, info->super_block->s_imap_blocks, info->super_block->s_block_size, info->inode_number);
   return 0;
}

void
setbitmap(bitchunk_t *bitmap, int nblocks, int blocksize, bit_t bitnr)
{
    uint64_t maxbits = nblocks * blocksize * 8;
    LOG("Setbitmap: %d\n", bitnr);
    if (bitnr > maxbits) {
        printf("Greater than max bits, should be less than %llu", maxbits + 1);
        return;
    }
    bitchunk_t *word = &bitmap[bitnr / (8 * sizeof(bitchunk_t))];
    (*word) |= (1 << (bitnr % blocksize));
}


void
unsetbitmap(bitchunk_t *bitmap, int nblocks, int blocksize, bit_t bitnr)
{
    uint64_t maxbits = nblocks * blocksize * 8;
    bitchunk_t mask;
    LOG("Setbitmap: %d\n", bitnr);
    if (bitnr > maxbits) {
        printf("Greater than max bits, should be less than %llu", maxbits + 1);
        return;
    }
    bitchunk_t *word = &bitmap[bitnr / (8 * sizeof(bitchunk_t))];
    mask = (1 << (bitnr % blocksize));
    (*word) &= (~mask);
}

int
convert_logical_to_physical_block (int device,
                                   struct super_block *superblock,
                                   d2_inode *d2inode,
                                   unsigned log_block,
                                   unsigned *ph_block)
{
    zone_t *block_array = d2inode->d2_zone;
    unsigned index = log_block;
    if (log_block >= INODE_BLOCKS(d2inode)) {
        return -1;
    }

    if (log_block >= V2_NR_DZONES) { //-- Indirect --//
        block_array = malloc(sizeof(zone_t) * V2_NR_TZONES);
        memcpy(block_array, d2inode->d2_zone, sizeof(zone_t) * V2_NR_TZONES );

        log_block -= V2_NR_DZONES;
        index = V2_NR_DZONES;

        if (log_block >= V2_INDIRECTS(BLOCKSIZE)) { //-- Double Indirect --//
            log_block -= V2_INDIRECTS(BLOCKSIZE);
            index = V2_NR_DZONES + 1;

            if (log_block >= ((V2_INDIRECTS(BLOCKSIZE)) * (V2_INDIRECTS(BLOCKSIZE)))) { //-- Triple Indirect --//
                log_block -= ((V2_INDIRECTS(BLOCKSIZE)) * (V2_INDIRECTS(BLOCKSIZE)));
                index = V2_NR_DZONES + 2;

                read_block(device, block_array[index], (char *) block_array, BLOCKSIZE);
                index = log_block / ((V2_INDIRECTS(BLOCKSIZE)) * (V2_INDIRECTS(BLOCKSIZE)));
                log_block -= index * ((V2_INDIRECTS(BLOCKSIZE)) * (V2_INDIRECTS(BLOCKSIZE)));
            }
            read_block(device, block_array[index],  (char *)block_array, BLOCKSIZE);

            index = log_block / V2_INDIRECTS(BLOCKSIZE);
            log_block -= index * V2_INDIRECTS(BLOCKSIZE);
        }

        read_block(device, block_array[index], (char *)block_array, BLOCKSIZE);
        index = log_block;
    }

    *ph_block = block_array[index];
    LOG("Set %d physical block\n", *ph_block);
    if (block_array != d2inode->d2_zone) {
        free(block_array);
    }

    return 0;
}


bool
check_root_inode_is_directory(int device, struct super_block *superblock)
{
    return check_inode_represents_directory(device, superblock, ROOT_INODE);
}

bool
check_inode_represents_directory(int device, struct super_block *superblock, int inode_number)
{
    d2_inode inode;
    if (inode_number <= 0) {
        return false;
    }
    get_inode(device, superblock, inode_number, &inode);
    return IS_DIR_INODE((&inode));
}

void
get_inode(int device, struct super_block *superblock, int inode_number, d2_inode *inode)
{
    read_block_from_offset(device, get_inode_block(superblock, inode_number), get_inode_offset(inode_number), (char *)inode, BLOCKSIZE, INODE_SIZE);
}

void
write_inode(int device, struct super_block *superblock, int inode_number, d2_inode *inode)
{
    d2_inode *disk_inode = malloc(BLOCKSIZE);
    int inode_block_number = get_inode_block(superblock, inode_number);
    int inode_block_index = get_inode_index(inode_number);
    LOG("offset: %d\n", inode_block_index);
    read_block(device, inode_block_number, (char *)disk_inode, BLOCKSIZE);
    for(int i = 0; i < V2_NR_TZONES; i++) {
        LOG("%d. %d\n", i, (disk_inode[inode_block_index]).d2_zone[i]);
    }

    disk_inode[inode_block_index] = *inode;
    write_block(device, inode_block_number, (char *) disk_inode, BLOCKSIZE);
}


int
corrupt_directory(int device, struct super_block *superblock, char *dirname)
{
    d2_inode inode;
    bitchunk_t *zbitmap;
    path_inode_t path_inode;
    path_inode.inode_number = 0;
    path_inode.path = dirname;
    struct direct *dp = malloc(BLOCKSIZE);
    custom_directory_walker(device, superblock, ROOT_INODE, "", 0, find_inode_for_path, &path_inode);
    LOG("Custom dir\n");
    if (path_inode.inode_number == 0) {
        return -1;
    }
    get_inode(device, superblock, path_inode.inode_number, &inode);
    inode.d2_size = (sizeof(DIR_ENTRY_SIZE) * 2); // For "." and ".."
    read_block(device, inode.d2_zone[0], (char *)dp, BLOCKSIZE);
    for (int i = 2; i < NR_DIR_ENTRIES(BLOCKSIZE); ++i) {
        LOG("zone %d: dir - %d, name: %s, inode %d\n", inode.d2_zone[0],i, dp[i].mfs_d_name, dp[i].mfs_d_ino, path_inode.inode_number);
        dp[i].mfs_d_ino = 0;
        strcpy(dp[i].mfs_d_name, "");
    }
    write_block(device , inode.d2_zone[0], (char *)dp, BLOCKSIZE);
    LOG("Inodenumber: %llu\n", path_inode.inode_number);
    for(int i = 1; i < V2_NR_TZONES; i++) {
        LOG("%d. %d\n", i, inode.d2_zone[i]);
        inode.d2_zone[i] = 0;
    }
    write_inode(device, superblock, path_inode.inode_number, &inode);
    zbitmap = alloc_bitmap(superblock->s_zmap_blocks);
    custom_inode_walker(device, superblock, imap, N_IMAP((superblock)), create_zone_bitmap, zbitmap);
    put_bitmap(device, zbitmap, BLK_ZMAP((superblock)), superblock->s_zmap_blocks);
    free(zbitmap);
    return 0;
}

void
read_block_from_offset(int device, int bno, int offset, char *buf, int block_size, int size)
{
    char *tmp_buf = malloc(block_size);
    memset(tmp_buf, 0, block_size);
    memset(buf, 0, size);
    if (offset >= block_size)
    {
        bno += offset/block_size;
        offset %= block_size;
    }
    read_block(device, bno, tmp_buf, block_size);
    memcpy(buf, &tmp_buf[offset], size);
    free(tmp_buf);
}

int get_inode_block(struct super_block *super_block, int inode_number)
{
    return (int)(((u64_t)(inode_number - 1) * INODE_SIZE) / BLOCKSIZE) + BLK_ILIST(super_block);
}

int get_inode_offset(int inode_number)
{
    return (int)(((u64_t)(inode_number - 1) * INODE_SIZE) % BLOCKSIZE);
}

int get_inode_index(int inode_number)
{
    return (inode_number - 1) % V2_INODES_PER_BLOCK(BLOCKSIZE);
}

int print_inode(int inode_number, d2_inode *inode)
{
    printf("Inode number: %d\n", inode_number);
    printf("Inode (links): %d\n", inode->d2_nlinks);
    printf("Inode (ctime): %d\n", inode->d2_ctime);
    printf("Inode (atime): %d\n", inode->d2_atime);
    printf("Inode (gid): %d\n", inode->d2_gid);
    printf("Inode (mode): %d\n", inode->d2_mode);
    printf("Inode (mtime): %d\n", inode->d2_mtime);
    printf("Inode (uid): %d\n", inode->d2_uid);
    printf("Inode (file Size): %d\n", inode->d2_size);
    PRINT_NEW_LINE;
    return 0;
}

int
zone_walker(int device, struct super_block *super_block, bitchunk_t *bitmap, int nblk)
{
    uint32_t bits_to_read = super_block->s_zones;
    uint32_t max_bits_in_chunk = sizeof(bitchunk_t) * 8;
    bit_t bit_nr = 0;
    bitchunk_t tmp_bitmap = *bitmap;
    while(bits_to_read) {
        if ((!(bit_nr % max_bits_in_chunk)) && (bits_to_read !=  super_block->s_zones)){
            bitmap += 1;
            tmp_bitmap = *bitmap;
        }
        if (((tmp_bitmap) & 1) == 1) {
            printf("Allocated Zone: %d\n", bit_nr);
        }
        bit_nr += 1;
        tmp_bitmap = tmp_bitmap >> 1;
        --bits_to_read;
    }
    return 0;
}



void
write_directory_entry(int device, struct super_block *superblock, uint32_t inode_number,  struct direct *dp) {
    d2_inode d2inode;
    uint64_t original_size;
    uint64_t logical_block;
    unsigned int physical_block_number;
    struct direct *physical_block = malloc(BLOCKSIZE);
    uint64_t dir_index_in_block;
    uint64_t total_entries;

    get_inode(device, superblock, inode_number, &d2inode);
    assert(IS_DIR_INODE((&d2inode)) == true);

    original_size = d2inode.d2_size;
    d2inode.d2_size += DIR_ENTRY_SIZE;
    logical_block = INODE_BLOCKS((&d2inode)) - 1;
    total_entries = (d2inode.d2_size) / DIR_ENTRY_SIZE;
    dir_index_in_block = total_entries % NR_DIR_ENTRIES(BLOCKSIZE);
    convert_logical_to_physical_block(device, superblock, &d2inode, logical_block, &physical_block_number);
    read_block(device, physical_block_number, (char *) physical_block, BLOCKSIZE);
    LOG("write dirent: inode_number %d, dir_index %d, phy dirname %s, phy inode %d\n", inode_number, dir_index_in_block,  physical_block[dir_index_in_block].mfs_d_name, physical_block[dir_index_in_block].mfs_d_ino);
    physical_block[dir_index_in_block] = *dp;
    LOG("write dirent: inode_number %d, dir_index %d, phy dirname %s, phy inode %d\n", inode_number, dir_index_in_block,  physical_block[dir_index_in_block].mfs_d_name, physical_block[dir_index_in_block].mfs_d_ino);
    write_block(device, physical_block_number, (char *) physical_block, BLOCKSIZE);
    write_inode(device, superblock, inode_number, &d2inode);
}

unsigned
div2(unsigned num1, unsigned num2)
{
    unsigned ans;

    ans = num1 / num2;
    if (num1 % num2)
        ans++;

    return ans;
}

