#include "common.h"

#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <stdint.h>
#include <sys/stat.h>
#include <unistd.h>

static status_t directory_walker(char *directory_name);
static status_t _directory_walker(char *directory_name, uint8_t level);

int
main(int argc, char **argv)
{
    char opt = 0;
    bool obtained_filename = false;
    status_t status = ok;
    if (argc < 2) {
        printf("usage: directory_walker -f filename\n");
        return -1;
    }

    while ((opt = getopt (argc, argv, "f:")) != -1) {
        switch (opt) {
        case 'f':
            obtained_filename = true;
            break;
        default:
            printf("usage: directory_walker -f filename\n");
            return -1;
        }
    }

    if (obtained_filename == true) {
        status = directory_walker(optarg);
        if (status != ok) {
            perror("Error");
        }
    }
    return 0;
}

status_t
directory_walker(char *directory_name)
{
    //ASSERT_NOT_NULL(directory_name);
    return _directory_walker(directory_name, 0);
}

status_t
_directory_walker(char *directory_name, uint8_t depth)
{
    DIR *dir = NULL;
    char path[1024];
    int len = 0;
    struct dirent *entry = NULL;
    struct stat stat_buffer;

    //ASSERT_NOT_NULL(directory_name);

    dir = opendir(directory_name);
    //NULL_CHECK_RETURN_ERROR(dir);

    while ((entry = readdir(dir))) {
        len = snprintf(path, sizeof(path)-1, "%s/%s", directory_name, entry->d_name);
        path[len] = 0;
        lstat(path, &stat_buffer);
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                continue;
            }
            printf("%*s[%s] (inode number: %ld) \n", depth*2, "", entry->d_name, (long)stat_buffer.st_ino);
            _directory_walker(path, depth + 1);
        } else {
            printf("%*s- %s (inode number: %ld)\n", depth * 2, "", entry->d_name, (long) stat_buffer.st_ino);
        } // Not a directory
    }

    closedir(dir);

    return ok;
}

