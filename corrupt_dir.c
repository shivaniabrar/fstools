//
// Created by Abrar Shivani on 4/25/17.
//

#include "mfs_structs.h"
#include "common.h"
#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <minix/config.h>
#include <minix/const.h>
#include <minix/type.h>
#include "mfs/const.h"
#include "mfs/inode.h"
#include "mfs/type.h"
#include "mfs/mfsdir.h"
#include <minix/fslib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include <assert.h>


#define CMD_USAGE \
printf("usage: corrupt_dir -f device -d directory\n")

int
main(int argc, char **argv) {
    int device = 0;
    struct super_block superblock;
    char opt = 0;

    bool obtained_filename = false;
    bool obtained_dir = false;

    char *device_path = NULL;
    char *dirpath = NULL;

    status_t status = ok;
    if (argc < 3) {
        CMD_USAGE;
        return -1;
    }

    while ((opt = getopt(argc, argv, "f:d:")) != -1) {
        switch (opt) {
            case 'f':
                obtained_filename = true;
                device_path = strdup(optarg);
                break;
            case 'd':
                obtained_dir = true;
                dirpath = strdup(optarg);
                break;
            default:
                CMD_USAGE;
                return -1;
        }
    }

    if (obtained_filename == false) {
        CMD_USAGE;
        fatal("require device path");
    }

    if (obtained_dir == false) {
        CMD_USAGE;
        fatal("require dir path");
    }

    if ((device = open(device_path, O_RDWR)) == -1) {
        perror("Could not open device file");
        exit(-1);
    }

    read_super_block(device, &superblock);
    get_bitmaps(&superblock);
    load_bitmap(device, imap, BLK_IMAP, N_IMAP((&superblock)));
    corrupt_directory(device, &superblock, dirpath);
    free(imap);
    free(zmap);
    close(device);
}